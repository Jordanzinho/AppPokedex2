//
//  SegundaViewController.swift
//  AppPokedex
//
//  Created by COTEMIG on 21/03/22.
//

import UIKit

class SegundaViewController: UIViewController {

    @IBAction func Voltar(_ sender: Any) {
        dismiss(animated: true)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        print("viewDidLoad - A tela foi carregada")

    }
    
    override func viewWillAppear(_ animated: Bool){
        super.viewWillAppear(animated)
        print("viewWillAppear - A tela vai ser exibida")
    }
    override func viewDidAppear(_ animated: Bool){
        super.viewDidAppear(animated)
        print("viewWillAppear - A tela foi exibida")
    }
    override func viewWillDisappear(_ animated: Bool){
        super.viewWillDisappear(animated)
        print("viewWillAppear - A tela vai desaparecer")
    }
    override func viewDidDisappear(_ animated: Bool){
        super.viewDidDisappear(animated)
        print("viewWillAppear - A tela desapareceu")
    }

}
